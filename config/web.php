<?php
use yii\helpers\ArrayHelper;

$basePath = dirname(__DIR__);
$params = require(__DIR__ . '/params.php');

Yii::setAlias('@app', $basePath);
Yii::setAlias('@vendor', $basePath . DIRECTORY_SEPARATOR . 'vendor');
Yii::setAlias('@runtime', $basePath . DIRECTORY_SEPARATOR . 'runtime');
Yii::setAlias('@webroot', $basePath . DIRECTORY_SEPARATOR . 'web');
Yii::setAlias('@uploads', $basePath . DIRECTORY_SEPARATOR . 'web/upload');

$config = [
    'id'         => 'basic',
    'name'       => 'Linex CPA',
    'basePath'   => $basePath,
    'bootstrap'  => ['log'],
    'components' => [
        'request' => [
            'cookieValidationKey' => 'ih66cv2yzFDhC05kpRd_Wm5jrvSoQVnS',
        ],
        'cache'   => file_exists(__DIR__ . '/cache-local.php')
            ? require(__DIR__ . '/cache-local.php')
            : ['class' => 'yii\caching\FileCache'],
        'user'    => [
            'identityClass'   => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'i18n'    => [
            'translations' => [
                'app' => [
                    'class'            => 'yii\i18n\PhpMessageSource',
                    'forceTranslation' => true,
                ],
            ],
        ],
        'mailer'  => [
            'class'            => 'yii\swiftmailer\Mailer',
            'useFileTransport' => true,
        ],
        'log'     => [
            'traceLevel' => YII_DEBUG ? 6 : 0,
            'targets'    => [
                [
                    'class'  => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
                [
                    'class'          => 'yii\log\FileTarget',
                    'exportInterval' => 1,
                    'categories'     => ['info'],
                    'levels'         => ['info'],
                    'logFile'        => '@runtime/logs/info.log',
                    'logVars'        => [],
                ],
            ],
        ],

        'urlManager' => [
            'enablePrettyUrl'     => true,
            'showScriptName'      => false,
            'enableStrictParsing' => true,
            'rules'               => [],
        ],

    ],
    'modules'    => [
        'main'     => file_exists(__DIR__ . '/server-local.php')
            ? require(__DIR__ . '/server-local.php')
            : [],
        'dynagrid' => [
            'class'           => '\kartik\dynagrid\Module',
            'dbSettings'      => [
                'tableName' => '{{%dynagrid}}',
            ],
            'dbSettingsDtl'   => [
                'tableName' => '{{%dynagrid_dtl}}',
            ],
            'dynaGridOptions' => [
                'storage'     => 'db',
                'gridOptions' => [
                    'toolbar' => [
                        '{dynagrid}',
                        '{toggleData}',
                        //'{export}',
                    ],
                    'export'  => false,

                ],
            ],

        ],
        'gridview' => [
            'class' => '\kartik\grid\Module',
        ],
    ],
    'params'     => $params,
];

if (file_exists(__DIR__ . '/db-local.php')) {
    $config['components']['db'] = require(__DIR__ . '/db-local.php');
}


if (YII_ENV_DEV) {
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
