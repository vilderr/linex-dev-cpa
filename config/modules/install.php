<?php
return [
    'bootstrap' => [
        'install',
        'linex\\modules\\main\\Bootstrap',
        'linex\\modules\\users\\Bootstrap',
        'linex\\modules\\dashboard\\Bootstrap',
        'linex\\modules\\pages\\Bootstrap',
        'linex\\modules\\catalog\\Bootstrap',
        'linex\\modules\\seo\\Bootstrap',
    ],
    'module'    => [
        'class'  => 'linex\\modules\\install\\Module',
        'layout' => 'main',
    ],
    'component' => [
        'errorHandler' => [
            'errorAction' => 'install/default/error',
        ],
    ],
    'rules'     => [
        [
            'class'       => 'yii\web\GroupUrlRule',
            'prefix'      => 'install',
            'routePrefix' => 'install',
            'rules'       => [
                ''             => 'default/index',
                'error'        => 'default/error',
                'set-language' => 'default/set-language',
                'requirements' => 'default/requirements',
                'environment'  => 'default/environment',
                'cache'        => 'default/cache',
                'db'           => 'default/db',
                'modules'      => 'default/modules',
                'user'         => 'default/user',
                'town'         => 'default/town',
                'complete'     => 'default/complete',
            ],
        ],
    ],
];