<?php
use linex\modules\main\components\AppManager;
// comment out the following two lines when deployed to production
//defined('YII_DEBUG') or define('YII_DEBUG', true);
//defined('YII_ENV') or define('YII_ENV', 'dev');

require(__DIR__ . '/../vendor/yiisoft/yii2/Yii.php');
$config = require(__DIR__ . '/../config/web.php');
require(__DIR__ . '/../vendor/autoload.php');
$manager = new AppManager();
$c = $manager->merge($config);

(new yii\web\Application($c))->run();
